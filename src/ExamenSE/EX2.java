package ExamenSE;

public class EX2 {
    public static void main(String[] args) {
        AThread AThread1 = new AThread();
        AThread AThread2 = new AThread();

        AThread1.setName("AThread1");
        AThread2.setName("AThread2");

        AThread1.start();
        AThread2.start();
    }
}

class AThread extends Thread{
    @Override
    public void run() {
        for(int i = 1; i < 13; i++){
            System.out.println(this.getName() + " - " + i);
            try{
                Thread.sleep(3000);
            }
            catch (Exception e){
                System.out.println("Error");;
            }
        }
    }
}